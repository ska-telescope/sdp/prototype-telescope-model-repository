SKA Mid PSI
--------------

These are configuration files for the SKA Mid Telescope in the PSI environment

File schemas are documented as indicated below:

File: ska-mid-cbf-system-parameters.json
*****************************************
Schema documented at:
https://developer.skao.int/projects/ska-telmodel/en/latest/schemas/ska-mid-cbf-initsysparam.html
File is checked against the schema in /test/test_mid_cbf_parameters.py

Note: the initial version of this file is an example only and should be updated to the values needed when in use in the PSI environment
