# Telescope Model Data (DEPRECATED)

This was a prototype repository for Telescope Model Data. It is deprecated as a
centralised location for data. Some data here have already been superseded, and
the intention is to archive/remove the repository when all remaining useful data
have been relocated.

The current sources of data are described in the Solution Intent page on
[Telescope Model Data](https://confluence.skatelescope.org/display/SWSI/Telescope+Model+Data).
