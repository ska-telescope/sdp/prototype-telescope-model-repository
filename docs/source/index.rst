.. ska-sdp-tmlite-repository documentation master file, created by
   sphinx-quickstart on Wed Apr 20 21:35:46 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

SKA SDP TMLite data repository
==============================

This project acts as the GitLab storage backend
of the :doc:`SKA SDP TMLite REST server <ska-sdp-tmlite-server:index>`.


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   rationale

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
